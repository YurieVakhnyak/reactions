const express = require("express");
const app = express();
const http = require("http").Server(app);
const io = require("socket.io")(http, {
  path: "/event-server",
  cors: ["http://localhost:3000/"],
});


app.use(express.static("public"));

let socketIsActive = false;

io.on("connection", (socket) => {
  console.log("User connected.");

  socket.on("disconnect", () => {
    console.log("User disconnected.");
  });

  socket.onAny((e, data) => {
    console.log("Event:", e);
    console.log("Data from event:", data);

    if (e === "control") {
      console.log("Inside control check:", e);
      console.log("Data from control component:", data);
      const { isDisplay } = data;
      socketIsActive = isDisplay;
    }
    if (e === "resetStats") {
      console.log("Inside reset check:", e);
      console.log("Data from reset component:", data);
      socket.broadcast.emit(e, data);
    }

    if (e === "isDisplayStats") {
      console.log("Inside isDisplayStats check:", e);
      console.log("Data from isDisplayStats component:", data);
      socket.broadcast.emit(e, data);
    }

    // if (e === "isDisplay") {
    //   console.log("Inside isDisplay check:", e);
    //   console.log("Data from isDisplay component:", data);
    //   socket.broadcast.emit(e, data);
    // }

    if (socketIsActive) {
      socket.broadcast.emit(e, data);
      console.log("data:", data, "e:", e);
    }
  });
});

const port = process.env.PORT || 4000;
http.listen(port, () => {
  console.log(`Server running on port ${port}`);
});
