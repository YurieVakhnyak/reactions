import { useEffect, useState } from "react";
import { socket } from "../../services/socket";
import Typography from "@mui/material/Typography";
import { Box } from "@mui/material";
import Badge from "@mui/material/Badge";
import css from "./OBSLayout.module.css";
import EmojiEmotionsTwoToneIcon from "@mui/icons-material/EmojiEmotionsTwoTone";

import { useContext } from "react";
import { ThemeContext } from "../../themes/ThemeWrapper";
import FormControlLabel from "@mui/material/FormControlLabel";
import Switch from "@mui/material/Switch";
// import { WrapText } from "@mui/icons-material";

const OBSLayoutStat = () => {
  const initialEmojis = {
    "💖": 0,
    "👍": 0,
    "🎉": 0,
    "👏": 0,
    "😂": 0,
    "😮": 0,
    "🤔": 0,
    "😢": 0,
    "👎": 0,
  };
  const [isDisplay, setIsDisplay] = useState(false);
  const [isDisplayStat, setIsDisplayStat] = useState(false);
  const [reactionsCount, setReactionsCount] = useState(initialEmojis);

  const { toggleTheme } = useContext(ThemeContext);
  const { themeMode } = useContext(ThemeContext);

  const totalReactionsCount = Object.values(reactionsCount).reduce(
    (acc, count) => acc + count,
    0
  );

  useEffect(() => {
    socket.on("message", (msg) => {
      console.log(msg);

      const parseData = JSON.parse(msg);
      const emoji = parseData.emoji;
      setReactionsCount((prevCount) => ({
        ...prevCount,
        [emoji]: prevCount[emoji] + 1,
      }));
    });

    socket.on("resetStats", (dataReset) => {
      if (dataReset === "reset") {
        setReactionsCount(initialEmojis);
      }
    });

    socket.on("isDisplayStats", (dataDisplayStat) => {
      setIsDisplayStat(dataDisplayStat);
    });

    // socket.on("isDisplay", (dataDisplay) => {
    //   setIsDisplay(dataDisplay);
    // });

    socket.on("control", (control) => {
      setIsDisplay(control.isDisplay);
      // setIsDisplayStat(control.isDisplayStat);
    });
  }, []);

  return (
    <>
      <Box display={isDisplay ? "none" : "block"}>
        <Typography>OBS Layout Statistics</Typography>
        <FormControlLabel
          control={<Switch defaultChecked />}
          label={themeMode === "dark" ? "Dark theme" : "Light theme"}
          onClick={toggleTheme}
        />
      </Box>

      <Box display={isDisplay ? "block" : "none"}>
        <Box
          display={isDisplayStat ? "flex" : "none"}
          style={{
            justifyContent: "center",
            gap: "1rem",
            position: "absolute",
            top: "20%",
            left: "0%",
            // transform: "translate(-50%, -50%)",
          }}
        >
          {Object.entries(reactionsCount).map(
            ([emoji, count]) =>
              count > 0 && (
                <Box key={emoji}>
                  <Badge badgeContent={count} max={9999} color="primary">
                    <span className={css.statEmoji}>{emoji}</span>
                  </Badge>
                </Box>
              )
          )}
          {totalReactionsCount > 0 && (
            <Box>
              <Badge
                badgeContent={totalReactionsCount}
                max={999}
                color="primary"
              >
                <EmojiEmotionsTwoToneIcon sx={{ fontSize: 28 }} color="error" />
              </Badge>
            </Box>
          )}
        </Box>
      </Box>
    </>
  );
};
export default OBSLayoutStat;
