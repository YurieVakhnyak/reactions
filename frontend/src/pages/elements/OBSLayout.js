import { useEffect, useState } from "react";
import { socket } from "../../services/socket";
import Typography from "@mui/material/Typography";
import { Box } from "@mui/material";
import { createEmoji } from "../../functions/functions";
import { createAnimatedEmojis } from "../../functions/functionCreateAnim";

import css from "./OBSLayout.module.css";

import Lottie from "lottie-react";
const confettiAnimation = require("../../animation/103110-confetti-pop.json");

import { useContext } from "react";
import { ThemeContext } from "../../themes/ThemeWrapper";
import FormControlLabel from "@mui/material/FormControlLabel";
import Switch from "@mui/material/Switch";
import { Button } from "@mui/material";
// import { WrapText } from "@mui/icons-material";

const OBSLayout = () => {
  const initialEmojis = {
    "💖": 0,
    "👍": 0,
    "🎉": 0,
    "👏": 0,
    "😂": 0,
    "😮": 0,
    "🤔": 0,
    "😢": 0,
    "👎": 0,
  };
  const [emojis, setEmojis] = useState([]);
  const [emojisAnimated, setEmojisAnimated] = useState([]);

  const [isDisplay, setIsDisplay] = useState(false);
  const [reactionsCount, setReactionsCount] = useState(initialEmojis);

  const { toggleTheme } = useContext(ThemeContext);
  const { themeMode } = useContext(ThemeContext);

  // const totalReactionsCount = Object.values(reactionsCount).reduce(
  //   (acc, count) => acc + count,
  //   0
  // );

  const handleClick = () => {
    createAnimatedEmojis("😮", setEmojisAnimated);
    console.log(emojisAnimated);
  };

  // const checkForAnimation = () => {
  //   for (const emoji in reactionsCount) {
  //     const count = reactionsCount[emoji];
  //     if (count % 10 === 0 && count !== 0) {
  //       createAnimatedEmojis(emoji, setEmojisAnimated);
  //       break;
  //     }
  //   }
  // };

  useEffect(() => {
    socket.on("message", (msg) => {
      console.log(msg);

      const parseData = JSON.parse(msg);
      const emoji = parseData.emoji;
      setReactionsCount((prevCount) => ({
        ...prevCount,
        [emoji]: prevCount[emoji] + 1,
      }));

      createEmoji(msg, setEmojis);
    });

    socket.on("resetStats", (dataReset) => {
      if (dataReset === "reset") {
        setReactionsCount(initialEmojis);
      }
    });

    socket.on("control", (control) => {
      setIsDisplay(control.isDisplay);
    });
  }, []);

  // useEffect(() => {
  //   checkForAnimation();
  // }, [reactionsCount]);

  return (
    <>
      <Box display={isDisplay ? "none" : "block"}>
        <Typography>OBS Layout</Typography>
        <FormControlLabel
          control={<Switch defaultChecked />}
          label={themeMode === "dark" ? "Dark theme" : "Light theme"}
          onClick={toggleTheme}
        />
      </Box>

      <Box display={isDisplay ? "block" : "none"}>
        <Button onClick={() => handleClick()}>Animate</Button>
        <Box>
          {reactionsCount["🎉"] % 10 === 0 && reactionsCount["🎉"] !== 0 && (
            <Lottie
              animationData={confettiAnimation}
              loop={false}
              style={{
                position: "absolute",
                top: "50%",
                left: "50%",
                transform: "translate(-50%, -50%)",
              }}
            />
          )}
        </Box>

        <Box>
          {emojis.map((emoji) => (
            <Box
              key={emoji.id}
              style={{
                top: window.innerHeight,
                left: emoji.left,
              }}
              className={css.emojiContainer}
            >
              <span className={css.emoji}>{emoji.emoji}</span>
              <span
                style={{
                  color: "#556cd6",
                }}
                className={css.emojiNick}
              >
                {emoji.user}
              </span>
            </Box>
          ))}
          {emojisAnimated.map((emijiAnim) => (
            <Box
              key={emijiAnim.id}
              style={{
                top: emijiAnim.top,
                left: emijiAnim.left,
              }}
              className={css.emojiContainer}
            >
              <span className={css.emojisAnimated}>
                {emijiAnim.emojisAnimated}
              </span>
            </Box>
          ))}
        </Box>
      </Box>
    </>
  );
};
export default OBSLayout;
