import {
  Button,
  Dialog,
  DialogTitle,
  DialogActions,
  Box,
  ToggleButton,
  ToggleButtonGroup,
  Container,
} from "@mui/material";
import { useEffect, useState } from "react";
import Typography from "@mui/material/Typography";
import { socket } from "../../services/socket";
// import { Button, Dialog, DialogTitle, DialogActions } from "@material-ui/core";

import { useContext } from "react";
import { ThemeContext } from "../../themes/ThemeWrapper";
import FormControlLabel from "@mui/material/FormControlLabel";
import Switch from "@mui/material/Switch";

const defaultEmojisList = [
  "💖",
  "👍",
  "🎉",
  "👏",
  "😂",
  "😮",
  "🤔",
  "😢",
  "👎",
];

const OBSControl = () => {
  const [isDisplay, setIsDisplay] = useState(false);
  const [isDisplayStat, setIsDisplayStat] = useState(false);
  const [emojiList, setEmojiList] = useState(defaultEmojisList);

  const { toggleTheme } = useContext(ThemeContext);
  const { themeMode } = useContext(ThemeContext);

  const [open, setOpen] = useState(false);

  const handleConfirm = () => {
    setOpen(false);
    socket.emit("resetStats", "reset");
  };

  const handleChange = (event) => {
    if (event.target.value === "false") {
      setIsDisplay(false);
      // change if it bug
      // socket.emit("isDisplay", isDisplay);
    }
    if (event.target.value === "true") {
      setIsDisplay(true);
      // change if it bug
      // socket.emit("isDisplay", isDisplay);
    }
  };

  const handleChangeStat = (event) => {
    if (event.target.value === "false") {
      setIsDisplayStat(false);
      socket.emit("isDisplayStats", !isDisplayStat);
    }
    if (event.target.value === "true") {
      setIsDisplayStat(true);
      socket.emit("isDisplayStats", !isDisplayStat);
    }
  };

  useEffect(() => {
    // Якщо потрібно при вмиканні віджету надсилати якісь додаткові дані окрім просто запуску віджета - дописувати їх у об'єкт
    // if (emojiList !== defaultEmojisList) {
    //   setEmojiList(newEmojiList);
    // }
    const resultOptions = { isDisplay, message: emojiList };

    socket.emit("control", resultOptions);
  }, [isDisplay, emojiList]);

  return (
    <Container
      sx={{
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        textAlign: "center",
        gap: "1rem",
        height: "393px",
        width: "208px",
        backgroundColor: themeMode === "dark" ? "#141414" : "#dddddd",
        // backgroundColor: "#eeeeee",
      }}
    >
      <Typography color="primary">OBS Control</Typography>

      <FormControlLabel
        sx={{
          color: "black",
          opacity: "0.7",
        }}
        control={<Switch defaultChecked />}
        label={themeMode === "dark" ? "Dark theme" : "Light theme"}
        onClick={toggleTheme}
      />

      <Box>
        <Typography color="primary">Widget</Typography>
        <ToggleButtonGroup
          color="primary"
          value={isDisplay}
          exclusive
          onChange={handleChange}
          aria-label="Platform"
        >
          <ToggleButton disabled={isDisplay} value={true}>
            Enable
          </ToggleButton>
          <ToggleButton disabled={!isDisplay} value={false}>
            Disable
          </ToggleButton>
        </ToggleButtonGroup>
      </Box>

      <Box>
        <Typography color="primary">Statistics</Typography>
        <ToggleButtonGroup
          color="primary"
          value={isDisplayStat}
          exclusive
          onChange={handleChangeStat}
          aria-label="Platform"
        >
          <ToggleButton disabled={isDisplayStat} value={true}>
            Show
          </ToggleButton>
          <ToggleButton disabled={!isDisplayStat} value={false}>
            Hide
          </ToggleButton>
        </ToggleButtonGroup>
      </Box>

      <Box>
        {open ? (
          <>
            <Button
              variant="outlined"
              color="primary"
              onClick={handleConfirm}
              style={{ marginRight: "1rem" }}
            >
              Yes
            </Button>
            <Button variant="outlined" onClick={() => setOpen(false)}>
              No
            </Button>
            <Typography
              sx={{
                color: "black",
                opacity: "0.7",
              }}
            >
              Reset?
            </Typography>
          </>
        ) : (
          <Button
            variant="outlined"
            color="primary"
            onClick={() => setOpen(true)}
          >
            Reset Statistics
          </Button>
        )}
      </Box>
    </Container>
  );
};
export default OBSControl;
