import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import { useState } from "react";
import { socket } from "../../services/socket";
import css from "./EventPageBlock.module.css";
import { useEffect } from "react";

import { useContext } from "react";
import { ThemeContext } from "../../themes/ThemeWrapper";
import FormControlLabel from "@mui/material/FormControlLabel";
import Switch from "@mui/material/Switch";

const defaultEmojisList = [
  "💖",
  "👍",
  "🎉",
  "👏",
  "😂",
  "😮",
  "🤔",
  "😢",
  "👎",
];

const EventPageBlock = () => {
  const [user, setUser] = useState("Vasyl Vasylyovich");
  // const [recievedMessage, setRecievedMessage] = useState("👍");
  const [emojisList, setEmojisList] = useState(defaultEmojisList);

  const { toggleTheme } = useContext(ThemeContext);
  const { themeMode } = useContext(ThemeContext);

  function handleClick(emoji) {
    let message = JSON.stringify({ emoji: emoji, user: user });

    socket.emit("message", message);
  }

  useEffect(() => {
    socket.on("control", (data) => {
      if (data.isDisplay) {
        setEmojisList(data.message);
        console.log("set List!");
      }
      if (data.message !== defaultEmojisList) {
        setEmojisList(data.message);
        console.log("set List!");
      }
    });
  }, [socket]);

  return (
    <>
      <div className={css.EventContainer}>
        {/* <div className={css.EventBox}> */}
        <FormControlLabel
          control={<Switch defaultChecked />}
          label={themeMode === "dark" ? "Dark" : "Light"}
          sx={{
            position: "absolute",
            top: "0",
            left: "5px",
          }}
          onClick={toggleTheme}
        />
        {/* <Typography color="primary" sx={{ fontSize: "1.5rem" }}>
          Click on an emoji to vote:
        </Typography> */}
        <Box
          sx={{
            "& button": { m: 1 },
            backgroundColor: "rgba(0, 0, 0, 0.7)",
            borderRadius: "2.3rem",
            height: "44px",
            display: "flex",
            // justifyContent: "center",
            alignItems: "center",
            margin: "auto",
          }}
          className={css.emojiBox}
        >
          {emojisList.map((emoji) => (
            <Button
              key={emoji}
              size="small"
              sx={{
                borderRadius: "50%",
                width: "28px",
                height: "28px",
                minWidth: "40px",
                minHeight: "40px",
              }}
              onClick={() => handleClick(emoji)}
            >
              <Typography sx={{ fontSize: "1.6rem" }}>{emoji}</Typography>
            </Button>
          ))}
        </Box>
        {/* </div> */}
      </div>
    </>
  );
};
export default EventPageBlock;
