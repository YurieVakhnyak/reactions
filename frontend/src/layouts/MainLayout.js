import * as React from "react";
import { Suspense } from "react";
import { Outlet } from "react-router-dom";
import Container from "@mui/material/Container";
import Menu from "../components/Menu";
import { Box } from "@mui/material";

import { useContext } from "react";
import { ThemeContext } from "../themes/ThemeWrapper";
import FormControlLabel from "@mui/material/FormControlLabel";
import Switch from "@mui/material/Switch";

export const MainLayout = () => {
  const { toggleTheme } = useContext(ThemeContext);
  const { themeMode } = useContext(ThemeContext);
  return (
    <>
      <Menu />
      <Container maxWidth="lg">
        <Box sx={{ my: 4 }}>
          <Suspense fallback={<div>Loading page...</div>}>
            <FormControlLabel
              control={<Switch defaultChecked />}
              label={themeMode === "dark" ? "Dark theme" : "Light theme"}
              onClick={toggleTheme}
            />
            <Outlet />
          </Suspense>
        </Box>
      </Container>
    </>
  );
};
