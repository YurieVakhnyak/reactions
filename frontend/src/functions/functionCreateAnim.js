export function createAnimatedEmojis(emojiForAnimation, setAnimatedEmojis) {
  const numEmojis = 10;

  for (let i = 0; i < numEmojis; i++) {
    const newAnimatedEmoji = {
      id: "animatedEmoji-" + Math.floor(Math.random() * 10 ** 17),
      top: Math.floor(Math.random() * window.innerHeight),
      left: Math.floor(Math.random() * window.innerWidth),
      emojisAnimated: emojiForAnimation,
    };
    // console.log("create emoji!!!");

    setAnimatedEmojis((prevState) => [...prevState, newAnimatedEmoji]);
    // console.log(newAnimatedEmoji);
    setTimeout(() => {
      setAnimatedEmojis((prevState) =>
        prevState.filter(
          (emojisAnimated) => emojisAnimated.id !== newAnimatedEmoji.id
        )
      );
    }, 3000);
  }
}
