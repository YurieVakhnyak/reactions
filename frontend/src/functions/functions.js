export function createEmoji(msg, setEmojis) {
  const parseData = JSON.parse(msg);
  const emoji = parseData.emoji;
  const user = parseData.user;

  const newEmoji = {
    id: "emoji-" + Math.floor(Math.random() * 10 ** 17),
    top: 0,
    left: Math.floor((Math.random() * window.innerWidth) / 3),
    emoji: emoji,
    user: user,
  };
  setEmojis((prevState) => [...prevState, newEmoji]);
  setTimeout(() => {
    setEmojis((prevState) =>
      prevState.filter((emoji) => emoji.id !== newEmoji.id)
    );
  }, 5000);
}

// export function createAnimatedEmojis(emojiForAnimation, setAnimatedEmojis) {
//   const numEmojis = 10;

//   for (let i = 0; i < numEmojis; i++) {
//     const newAnimatedEmoji = {
//       id: "animatedEmoji-" + Math.floor(Math.random() * 10 ** 17),
//       top: Math.floor(Math.random() * window.innerHeight),
//       left: Math.floor(Math.random() * window.innerWidth),
//       emojiForAnimation: emojiForAnimation,
//     };

//     setAnimatedEmojis((prevState) => [...prevState, newAnimatedEmoji]);

//     setTimeout(() => {
//       setAnimatedEmojis((prevState) =>
//         prevState.filter(
//           (animatedEmoji) => animatedEmoji.id !== newAnimatedEmoji.id
//         )
//       );
//     }, 3000);
//   }
// }
